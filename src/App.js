import React, { Component } from 'react';
import { View, Text } from 'react-native';
import firebase from 'firebase';
import { Header } from './components/common/index';
import LoginForm from './components/LoginForm';

class App extends Component {
    componentWillMount(){
        firebase.initializeApp({
            apiKey: 'AIzaSyBqt6pIh4lirskG1kIAojDFpUU45XMSHSQ',
            authDomain: 'auth-63238.firebaseapp.com',
            databaseURL: 'https://auth-63238.firebaseio.com',
            projectId: 'auth-63238',
            storageBucket: 'auth-63238.appspot.com',
            messagingSenderId: '353814067585'
          });
    }

    render() {
        return (
            <View>
                <Header headerText="Authentication" />
                <LoginForm/>
            </View>
        );
    }
}

export default App;